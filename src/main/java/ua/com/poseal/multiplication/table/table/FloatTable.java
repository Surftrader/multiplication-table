package ua.com.poseal.multiplication.table.table;

import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static ua.com.poseal.multiplication.table.App.*;

public class FloatTable extends Table<Float> {

    public FloatTable(Properties properties) {
        super(properties);
    }

    @Override
    public Float multiply(Float num1, Float num2) {
        float result = num1 * num2;
        if (Float.isInfinite(result)) {
            throw new AppException("Out of range");
        }
        return result;
    }

    @Override
    public boolean isIncrementNegative() {
        return increment < 0;
    }


    @Override
    public void initProperties(Properties properties) {
        try {
            min = Float.parseFloat(properties.getProperty(MIN));
            max = Float.parseFloat(properties.getProperty(MAX));
            increment = Float.parseFloat(properties.getProperty(INCREMENT));

            if (Float.isInfinite(min) || Float.isInfinite(max) || Float.isInfinite(increment)) {
                throw new AppException("Invalid properties");
            }
        } catch (NumberFormatException e) {
            throw new AppException("Invalid properties");
        }
    }

    @Override
    public Float addIncrement(Float value, Float increment) {
        return value + increment;
    }

    @Override
    public Float clone(Float aFloat) {
        return Float.valueOf(String.valueOf(aFloat));
    }
}
