package ua.com.poseal.multiplication.table.table;

import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static ua.com.poseal.multiplication.table.App.*;

public class IntegerTable extends Table<Integer> {

    public IntegerTable(Properties properties) {
        super(properties);
    }

    @Override
    public Integer multiply(Integer num1, Integer num2) {
        long result = (long) num1 * num2;
        if (result > Integer.MAX_VALUE || result < Integer.MIN_VALUE) {
            throw new AppException("Out of range");
        }
        return (int) result;
    }

    @Override
    public boolean isIncrementNegative() {
        return increment < 0;
    }

    @Override
    public void initProperties(Properties properties) {
        try {
            min = Integer.parseInt(properties.getProperty(MIN));
            max = Integer.parseInt(properties.getProperty(MAX));
            increment = Integer.parseInt(properties.getProperty(INCREMENT));
        } catch (NumberFormatException e) {
            throw new AppException("Invalid properties");
        }
    }

    @Override
    public Integer addIncrement(Integer value, Integer increment) {
        return value + increment;
    }

    @Override
    public Integer clone(Integer integer) {
        return new Integer(String.valueOf(integer));
    }
}
