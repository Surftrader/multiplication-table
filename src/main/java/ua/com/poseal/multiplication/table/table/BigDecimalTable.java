package ua.com.poseal.multiplication.table.table;

import ua.com.poseal.multiplication.table.exception.AppException;

import java.math.BigDecimal;
import java.util.Properties;

import static ua.com.poseal.multiplication.table.App.*;

public class BigDecimalTable extends Table<BigDecimal> {

    public BigDecimalTable(Properties properties) {
        super(properties);
    }

    @Override
    public BigDecimal multiply(BigDecimal num1, BigDecimal num2) {
        return num1.multiply(num2);
    }

    @Override
    public boolean isIncrementNegative() {
        return increment.compareTo(BigDecimal.valueOf(0)) < 0;
    }

    @Override
    public void initProperties(Properties properties) {
        try {
            min = new BigDecimal(properties.getProperty(MIN));
            max = new BigDecimal(properties.getProperty(MAX));
            increment = new BigDecimal(properties.getProperty(INCREMENT));
        } catch (NumberFormatException e) {
            throw new AppException("Invalid properties");
        }
    }

    @Override
    public BigDecimal addIncrement(BigDecimal value, BigDecimal increment) {
        return value.add(increment);
    }

    @Override
    public BigDecimal clone(BigDecimal bigDecimal) {
        return new BigDecimal(String.valueOf(bigDecimal));
    }
}
