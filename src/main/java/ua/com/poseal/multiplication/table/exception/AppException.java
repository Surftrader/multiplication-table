package ua.com.poseal.multiplication.table.exception;

public class AppException extends RuntimeException {
    public AppException(String message) {
        super(message);
    }
}
