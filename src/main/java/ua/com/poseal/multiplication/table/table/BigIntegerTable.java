package ua.com.poseal.multiplication.table.table;

import ua.com.poseal.multiplication.table.exception.AppException;

import java.math.BigInteger;
import java.util.Properties;

import static ua.com.poseal.multiplication.table.App.*;

public class BigIntegerTable extends Table<BigInteger> {

    public BigIntegerTable(Properties properties) {
        super(properties);
    }

    @Override
    public BigInteger multiply(BigInteger num1, BigInteger num2) {
        return num1.multiply(num2);
    }

    @Override
    public boolean isIncrementNegative() {
        return increment.compareTo(BigInteger.valueOf(0)) < 0;
    }

    @Override
    public void initProperties(Properties properties) {
        try {
            min = new BigInteger(properties.getProperty(MIN));
            max = new BigInteger(properties.getProperty(MAX));
            increment = new BigInteger(properties.getProperty(INCREMENT));
        } catch (NumberFormatException e) {
            throw new AppException("Invalid properties");
        }
    }

    @Override
    public BigInteger addIncrement(BigInteger value, BigInteger increment) {
        return value.add(increment);
    }

    @Override
    public BigInteger clone(BigInteger bigInteger) {
        return new BigInteger(String.valueOf(bigInteger));
    }
}
