package ua.com.poseal.multiplication.table.table;

import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static ua.com.poseal.multiplication.table.App.*;


public class DoubleTable extends Table<Double> {

    public DoubleTable(Properties properties) {
        super(properties);
    }

    @Override
    public Double multiply(Double num1, Double num2) {
        double result = num1 * num2;
        if (Double.isInfinite(result)) {
            throw new AppException("Out of range");
        }
        return result;
    }

    @Override
    public boolean isIncrementNegative() {
        return increment < 0;
    }

    @Override
    public void initProperties(Properties properties) {
        try {
            min = Double.parseDouble(properties.getProperty(MIN));
            max = Double.parseDouble(properties.getProperty(MAX));
            increment = Double.parseDouble(properties.getProperty(INCREMENT));
            if (Double.isInfinite(min) || Double.isInfinite(max) || Double.isInfinite(increment)) {
                throw new AppException("Invalid properties");
            }
        } catch (NumberFormatException e) {
            throw new AppException("Invalid properties");
        }
    }

    @Override
    public Double addIncrement(Double value, Double increment) {
        return value + increment;
    }

    @Override
    public Double clone(Double aDouble) {
        return Double.valueOf(String.valueOf(aDouble));
    }
}
