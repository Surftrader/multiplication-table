package ua.com.poseal.multiplication.table;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.com.poseal.multiplication.table.exception.AppException;
import ua.com.poseal.multiplication.table.table.Table;
import ua.com.poseal.multiplication.table.table.TableFactory;
import ua.com.poseal.multiplication.table.util.Loader;

import java.util.Optional;
import java.util.Properties;

import static ua.com.poseal.multiplication.table.table.TableFactory.INT;

public class App {
    public static final Logger logger = LoggerFactory.getLogger("LOGGER");
    public static final Logger printer = LoggerFactory.getLogger("PRINT");
    public static final Logger log = LoggerFactory.getLogger("FILE");

    public static final String DATA_TYPE = "type";
    public static final String MIN = "min";
    public static final String MAX = "max";
    public static final String INCREMENT = "increment";

    public static void main(String[] args) {
        try {
            App app = new App();
            app.run();
        } catch (AppException e) {
            printer.error(e.getMessage());
        }
    }

    private void run() {
        logger.debug("Entered run() method");
        Properties properties = new Loader().getFileProperties();
        logger.info("Get properties");

        String type = Optional.ofNullable(System.getProperty(DATA_TYPE)).orElse(INT);
        logger.info("Get {}={} from system property", DATA_TYPE, type);

        properties.setProperty(DATA_TYPE, type);
        logger.info("Set {}={} to properties", DATA_TYPE, type);

        Table<?> table = TableFactory.getTable(properties);
        logger.info("Table {} was created", table.getClass());

        table.print();
        logger.debug("Exited run() method");
    }
}
