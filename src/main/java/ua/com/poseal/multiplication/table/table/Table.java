package ua.com.poseal.multiplication.table.table;

import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static ua.com.poseal.multiplication.table.App.*;

public abstract class Table<T extends Number & Comparable<T>> {

    public T min;
    public T max;
    public T increment;

    public Table(Properties properties) {
        initProperties(properties);
        checkConsistent();
    }

    protected void checkConsistent() {
        logger.debug("Entered checkConsistent() method");
        if (min.compareTo(max) > 0) {
            logger.error("Min value is more than max value");
            throw new AppException("Min value must be less than or equal to max value");
        }
        if (isIncrementNegative()) {
            logger.error("Increment value is less than 0");
            throw new AppException("Min value must be more than 0");
        }
        logger.debug("Exited checkConsistent() method");
    }

    public abstract boolean isIncrementNegative();

    public void print() {
        logger.debug("Entered print() method");
        for (T a = clone(min); a.compareTo(max) <= 0; a = addIncrement(a, increment)) {
            for (T b = clone(min); b.compareTo(max) <= 0; b = addIncrement(b, increment)) {
                printer.info("{} * {} = {} ", a, b, multiply(a, b));
                log.info("{} * {} = {} ", a, b, multiply(a, b));
            }
            printer.info(" ");
        }
        logger.debug("Exited print() method");
    }

    public abstract void initProperties(Properties properties);

    public abstract T multiply(T t1, T t2);

    public abstract T addIncrement(T value, T increment);

    public abstract T clone(T t);

}
