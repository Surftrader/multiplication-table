package ua.com.poseal.multiplication.table.table;

import java.util.Properties;

import static ua.com.poseal.multiplication.table.App.DATA_TYPE;
import static ua.com.poseal.multiplication.table.App.logger;

public class TableFactory {
    public static final String BYTE = "byte";
    public static final String SHORT = "short";
    public static final String INT = "int";
    public static final String LONG = "long";
    public static final String FLOAT = "float";
    public static final String DOUBLE = "double";
    public static final String BIG_INTEGER = "bigInteger";
    public static final String BIG_DECIMAL = "bigDecimal";
    public static Table<?> getTable(Properties properties) {
        logger.debug("Entered to getTable() method with arguments: properties={}", properties);

        String type = properties.getProperty(DATA_TYPE);
        logger.info("Data type from properties = {}", type);
        if (type == null) type = INT;

        switch (type) {
            case BYTE:
                return new ByteTable(properties);
            case SHORT:
                return new ShortTable(properties);
            case LONG:
                return new LongTable(properties);
            case FLOAT:
                return new FloatTable(properties);
            case DOUBLE:
                return new DoubleTable(properties);
            case BIG_INTEGER:
                return new BigIntegerTable(properties);
            case BIG_DECIMAL:
                return new BigDecimalTable(properties);
            default:
                return new IntegerTable(properties);
        }
    }
}
