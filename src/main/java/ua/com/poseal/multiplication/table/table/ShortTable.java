package ua.com.poseal.multiplication.table.table;

import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static ua.com.poseal.multiplication.table.App.*;


public class ShortTable extends Table<Short> {

    public ShortTable(Properties properties) {
        super(properties);
    }

    @Override
    public Short multiply(Short num1, Short num2) {
        int result = num1 * num2;
        if (result > Short.MAX_VALUE || result < Short.MIN_VALUE) {
            throw new AppException("Out of range");
        }
        return (short) result;
    }

    @Override
    public boolean isIncrementNegative() {
        return increment < 0;
    }

    @Override
    public void initProperties(Properties properties) {
        try {
            min = Short.parseShort(properties.getProperty(MIN));
            max = Short.parseShort(properties.getProperty(MAX));
            increment = Short.parseShort(properties.getProperty(INCREMENT));
        } catch (NumberFormatException e) {
            throw new AppException("Invalid properties");
        }
    }

    @Override
    public Short addIncrement(Short value, Short increment) {
        return (short) (value + increment);
    }

    @Override
    public Short clone(Short aShort) {
        return new Short((short) aShort);
    }
}
