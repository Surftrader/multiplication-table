package ua.com.poseal.multiplication.table.table;

import ua.com.poseal.multiplication.table.exception.AppException;

import java.math.BigInteger;
import java.util.Properties;

import static ua.com.poseal.multiplication.table.App.*;


public class LongTable extends Table<Long> {

    public LongTable(Properties properties) {
        super(properties);
    }

    @Override
    public Long multiply(Long num1, Long num2) {
        BigInteger result = BigInteger.valueOf(num1).multiply(BigInteger.valueOf(num2));
        if (result.compareTo(BigInteger.valueOf(Long.MAX_VALUE)) > 0
                || result.compareTo(BigInteger.valueOf(Long.MIN_VALUE)) < 0) {
            throw new AppException("Out of range");
        }
        return result.longValue();
    }

    @Override
    public boolean isIncrementNegative() {
        return increment < 0;
    }

    @Override
    public void initProperties(Properties properties) {
        try {
            min = Long.parseLong(properties.getProperty(MIN));
            max = Long.parseLong(properties.getProperty(MAX));
            increment = Long.parseLong(properties.getProperty(INCREMENT));
        } catch (NumberFormatException e) {
            throw new AppException("Invalid properties");
        }
    }

    @Override
    public Long addIncrement(Long value, Long increment) {
        return value + increment;
    }

    @Override
    public Long clone(Long aLong) {
        return new Long(String.valueOf(aLong));
    }
}
