package ua.com.poseal.multiplication.table.table;

import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static ua.com.poseal.multiplication.table.App.*;


public class ByteTable extends Table<Byte> {

    public ByteTable(Properties properties) {
        super(properties);
    }

    @Override
    public Byte multiply(Byte num1, Byte num2) {
        int result = num1 * num2;
        if (result > Byte.MAX_VALUE || result < Byte.MIN_VALUE) {
            throw new AppException("Out of range");
        }
        return (byte) result;
    }

    @Override
    public boolean isIncrementNegative() {
        return increment < 0;
    }

    @Override
    public void initProperties(Properties properties) {
        try {
            min = Byte.parseByte(properties.getProperty(MIN));
            max = Byte.parseByte(properties.getProperty(MAX));
            increment = Byte.parseByte(properties.getProperty(INCREMENT));
        } catch (NumberFormatException e) {
            throw new AppException("Invalid properties");
        }
    }

    @Override
    public Byte addIncrement(Byte value, Byte increment) {
        return (byte) (value + increment);
    }

    @Override
    public Byte clone(Byte aByte) {
        return new Byte(String.valueOf(aByte));
    }
}
