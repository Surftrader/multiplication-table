package ua.com.poseal.multiplication.table.table;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static ua.com.poseal.multiplication.table.App.*;

class DoubleTableTest {

    private Properties properties;

    @BeforeEach
    void initData() {
        properties = new Properties();
        properties.setProperty(MIN, "1");
        properties.setProperty(MAX, "2");
        properties.setProperty(INCREMENT, "1");
    }

    @Test
    void whenConstructorValidThenTestPasses() {
        Table<Double> doubleTable = new DoubleTable(properties);
        assertEquals(Double.parseDouble(properties.getProperty(MIN)), doubleTable.min);
        assertEquals(Double.parseDouble(properties.getProperty(MAX)), doubleTable.max);
        assertEquals(Double.parseDouble(properties.getProperty(INCREMENT)), doubleTable.increment);
    }

    @Test
    void whenConstructorNotValidThenThrowException() {
        properties.setProperty(MIN, "A");
        assertThrows(AppException.class, () -> new DoubleTable(properties), "Invalid properties");

        properties.setProperty(MAX, "B");
        assertThrows(AppException.class, () -> new DoubleTable(properties), "Invalid properties");

        properties.setProperty(INCREMENT, "C");
        assertThrows(AppException.class, () -> new DoubleTable(properties), "Invalid properties");
    }

    @Test
    void whenIncrementLessZeroThenThrowException() {
        properties.setProperty(INCREMENT, "-1");
        assertThrows(AppException.class,
                () -> new DoubleTable(properties),
                "Min value must be more than 0");
    }

    @Test
    void whenMinMoreMaxThenThrowException() {
        properties.setProperty(MIN, "10");
        assertThrows(AppException.class,
                () -> new DoubleTable(properties),
                "Min value must be less than or equal to max value");
    }

    @Test
    void whenMaxMoreThenMaxByteValueThenThrowException() {
        properties.setProperty(MAX, String.valueOf(Double.POSITIVE_INFINITY));
        assertThrows(AppException.class, () -> new DoubleTable(properties), "Invalid properties");
    }

    @Test
    void whenMinLessThenMinByteValueThenThrowException() {
        properties.setProperty(MIN, String.valueOf(Double.NEGATIVE_INFINITY));
        assertThrows(AppException.class, () -> new DoubleTable(properties), "Invalid properties");
    }

    @Test
    void whenResultOfMultiplicationIsNotOutOfRangeThenReturnResult() {
        Table<Double> doubleTable = new DoubleTable(properties);
        double a = 1;
        double b = 2;
        assertEquals(a * b, doubleTable.multiply(a, b), 0.01);
    }

    @Test
    void whenResultOfMultiplicationIsOutOfRangeThenThrowException() {
        Table<Double> doubleTable = new DoubleTable(properties);
        assertThrows(AppException.class,
                () -> doubleTable.multiply(Double.MAX_VALUE, Double.MAX_VALUE),
                "Out of range");
    }

    @Test
    void whenAddIncrementThenNumberIsIncremented() {
        Table<Double> doubleTable = new DoubleTable(properties);
        double a = 120;
        assertEquals(a + Double.parseDouble(properties.getProperty(INCREMENT)),
                doubleTable.addIncrement(a, doubleTable.increment));
    }

    @Test
    void whenCloneThenReturnThereAreDifferentObjectsWithTheSameValue() {
        Table<Double> doubleTable = new DoubleTable(properties);
        double a = 120;
        assertNotSame(a, doubleTable.clone(a));
        assertEquals(a, doubleTable.clone(a));
    }
}
