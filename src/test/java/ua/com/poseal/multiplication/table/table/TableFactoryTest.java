package ua.com.poseal.multiplication.table.table;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static ua.com.poseal.multiplication.table.App.*;
import static ua.com.poseal.multiplication.table.table.TableFactory.*;


class TableFactoryTest {
    private final Properties properties = new Properties();

    @BeforeEach
    void setUp() {
        properties.setProperty(MIN, "1");
        properties.setProperty(MAX, "2");
        properties.setProperty(INCREMENT, "1");
    }

    @Test
    void getTableDependingOnTypeProperty() {

        assertInstanceOf(IntegerTable.class, TableFactory.getTable(properties));

        properties.setProperty(DATA_TYPE, BYTE);
        assertInstanceOf(ByteTable.class, TableFactory.getTable(properties));

        properties.setProperty(DATA_TYPE, SHORT);
        assertInstanceOf(ShortTable.class, TableFactory.getTable(properties));

        properties.setProperty(DATA_TYPE, INT);
        assertInstanceOf(IntegerTable.class, TableFactory.getTable(properties));

        properties.setProperty(DATA_TYPE, LONG);
        assertInstanceOf(LongTable.class, TableFactory.getTable(properties));

        properties.setProperty(DATA_TYPE, FLOAT);
        assertInstanceOf(FloatTable.class, TableFactory.getTable(properties));

        properties.setProperty(DATA_TYPE, DOUBLE);
        assertInstanceOf(DoubleTable.class, TableFactory.getTable(properties));

        properties.setProperty(DATA_TYPE, BIG_INTEGER);
        assertInstanceOf(BigIntegerTable.class, TableFactory.getTable(properties));

        properties.setProperty(DATA_TYPE, BIG_DECIMAL);
        assertInstanceOf(BigDecimalTable.class, TableFactory.getTable(properties));
    }
}