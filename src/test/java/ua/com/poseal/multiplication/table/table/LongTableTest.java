package ua.com.poseal.multiplication.table.table;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static ua.com.poseal.multiplication.table.App.*;

class LongTableTest {
    private Properties properties;

    @BeforeEach
    void initData() {
        properties = new Properties();
        properties.setProperty(MIN, "1");
        properties.setProperty(MAX, "2");
        properties.setProperty(INCREMENT, "1");
    }

    @Test
    void whenConstructorValidThenTestPasses() {
        Table<Long> longTable = new LongTable(properties);
        assertEquals(Long.parseLong(properties.getProperty(MIN)), longTable.min);
        assertEquals(Long.parseLong(properties.getProperty(MAX)), longTable.max);
        assertEquals(Long.parseLong(properties.getProperty(INCREMENT)), longTable.increment);
    }

    @Test
    void whenConstructorNotValidThenThrowException() {
        properties.setProperty(MIN, "A");
        assertThrows(AppException.class, () -> new LongTable(properties), "Invalid properties");

        properties.setProperty(MAX, "B");
        assertThrows(AppException.class, () -> new LongTable(properties), "Invalid properties");

        properties.setProperty(INCREMENT, "C");
        assertThrows(AppException.class, () -> new LongTable(properties), "Invalid properties");
    }

    @Test
    void whenIncrementLessZeroThenThrowException() {
        properties.setProperty(INCREMENT, "-1");
        assertThrows(AppException.class,
                () -> new LongTable(properties),
                "Min value must be more than 0");
    }

    @Test
    void whenMinMoreMaxThenThrowException() {
        properties.setProperty(MIN, "10");
        assertThrows(AppException.class,
                () -> new LongTable(properties),
                "Min value must be less than or equal to max value");
    }

    @Test
    void whenMaxMoreThenMaxByteValueThenThrowException() {
        properties.setProperty(MAX, String.valueOf(Long.MAX_VALUE + 1));
        assertThrows(AppException.class, () -> new LongTable(properties));
    }

    @Test
    void whenMinLessThenMinByteValueThenThrowException() {
        properties.setProperty(MIN, String.valueOf(Long.MIN_VALUE - 1));
        assertThrows(AppException.class, () -> new LongTable(properties));
    }

    @Test
    void whenResultOfMultiplicationIsNotOutOfRangeThenReturnResult() {
        Table<Long> longTable = new LongTable(properties);
        long a = 1;
        long b = 2;
        assertEquals(a * b, longTable.multiply(a, b));
    }

    @Test
    void whenResultOfMultiplicationIsOutOfRangeThenThrowException() {
        Table<Long> longTable = new LongTable(properties);
        assertThrows(AppException.class,
                () -> longTable.multiply(Long.MAX_VALUE, Long.MAX_VALUE),
                "Out of range");
    }

    @Test
    void whenAddIncrementThenNumberIsIncremented() {
        Table<Long> longTable = new LongTable(properties);
        long a = 120;
        assertEquals(a + Long.parseLong(properties.getProperty(INCREMENT)),
                longTable.addIncrement(a, longTable.increment));
    }

    @Test
    void whenCloneThenReturnThereAreDifferentObjectsWithTheSameValue() {
        Table<Long> longTable = new LongTable(properties);
        long a = 120;
        assertNotSame(a, longTable.clone(a));
        assertEquals(a, longTable.clone(a));
    }

}