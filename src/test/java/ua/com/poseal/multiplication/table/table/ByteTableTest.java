package ua.com.poseal.multiplication.table.table;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static ua.com.poseal.multiplication.table.App.*;

class ByteTableTest {

    private Properties properties;

    @BeforeEach
    void initData() {
        properties = new Properties();
        properties.setProperty(MIN, "1");
        properties.setProperty(MAX, "2");
        properties.setProperty(INCREMENT, "1");
    }

    @Test
    void whenConstructorValidThenTestPasses() {
        Table<Byte> byteTable = new ByteTable(properties);
        assertEquals(Byte.parseByte(properties.getProperty(MIN)), byteTable.min);
        assertEquals(Byte.parseByte(properties.getProperty(MAX)), byteTable.max);
        assertEquals(Byte.parseByte(properties.getProperty(INCREMENT)), byteTable.increment);
    }

    @Test
    void whenConstructorNotValidThenThrowException() {
        properties.setProperty(MIN, "A");
        assertThrows(AppException.class, () -> new ByteTable(properties), "Invalid properties");

        properties.setProperty(MAX, "B");
        assertThrows(AppException.class, () -> new ByteTable(properties), "Invalid properties");

        properties.setProperty(INCREMENT, "C");
        assertThrows(AppException.class, () -> new ByteTable(properties), "Invalid properties");
    }

    @Test
    void whenIncrementLessZeroThenThrowException() {
        properties.setProperty(INCREMENT, "-1");
        assertThrows(AppException.class,
                () -> new ByteTable(properties),
                "Min value must be more than 0");
    }

    @Test
    void whenMinMoreMaxThenThrowException() {
        properties.setProperty(MIN, "10");
        assertThrows(AppException.class,
                () -> new ByteTable(properties),
                "Min value must be less than or equal to max value");
    }

    @Test
    void whenMaxMoreThenMaxByteValueThenThrowException() {
        properties.setProperty(MAX, String.valueOf(Byte.MAX_VALUE + 1));
        assertThrows(AppException.class, () -> new ByteTable(properties));
    }

    @Test
    void whenMinLessThenMinByteValueThenThrowException() {
        properties.setProperty(MIN, String.valueOf(Byte.MIN_VALUE - 1));
        assertThrows(AppException.class, () -> new ByteTable(properties));
    }

    @Test
    void whenResultOfMultiplicationIsNotOutOfRangeThenReturnResult() {
        Table<Byte> byteTable = new ByteTable(properties);
        byte a = 1;
        byte b = 2;
        assertEquals((byte) (a * b), byteTable.multiply(a, b));
    }

    @Test
    void whenResultOfMultiplicationIsOutOfRangeThenThrowException() {
        Table<Byte> byteTable = new ByteTable(properties);
        assertThrows(AppException.class,
                () -> byteTable.multiply(Byte.MAX_VALUE, Byte.MAX_VALUE),
                "Out of range");
    }

    @Test
    void whenAddIncrementThenNumberIsIncremented() {
        Table<Byte> byteTable = new ByteTable(properties);
        byte a = 120;
        assertEquals((byte) (a + Byte.parseByte(properties.getProperty(INCREMENT))),
                byteTable.addIncrement(a, byteTable.increment));
    }

    @Test
    void whenCloneThenReturnThereAreDifferentObjectsWithTheSameValue() {
        Table<Byte> byteTable = new ByteTable(properties);
        byte a = 120;
        assertNotSame(a, byteTable.clone(a));
        assertEquals(a, byteTable.clone(a));
    }
}
