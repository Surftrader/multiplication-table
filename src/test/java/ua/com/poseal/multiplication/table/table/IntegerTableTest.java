package ua.com.poseal.multiplication.table.table;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static ua.com.poseal.multiplication.table.App.*;

class IntegerTableTest {

    private Properties properties;

    @BeforeEach
    void initData() {
        properties = new Properties();
        properties.setProperty(MIN, "1");
        properties.setProperty(MAX, "2");
        properties.setProperty(INCREMENT, "1");
    }

    @Test
    void whenConstructorValidThenTestPasses() {
        Table<Integer> integerTable = new IntegerTable(properties);
        assertEquals(Byte.parseByte(properties.getProperty(MIN)), integerTable.min);
        assertEquals(Byte.parseByte(properties.getProperty(MAX)), integerTable.max);
        assertEquals(Byte.parseByte(properties.getProperty(INCREMENT)), integerTable.increment);
    }

    @Test
    void whenConstructorNotValidThenThrowException() {
        properties.setProperty(MIN, "A");
        assertThrows(AppException.class, () -> new IntegerTable(properties), "Invalid properties");

        properties.setProperty(MAX, "B");
        assertThrows(AppException.class, () -> new IntegerTable(properties), "Invalid properties");

        properties.setProperty(INCREMENT, "C");
        assertThrows(AppException.class, () -> new IntegerTable(properties), "Invalid properties");
    }

    @Test
    void whenIncrementLessZeroThenThrowException() {
        properties.setProperty(INCREMENT, "-1");
        assertThrows(AppException.class,
                () -> new IntegerTable(properties),
                "Min value must be more than 0");
    }

    @Test
    void whenMinMoreMaxThenThrowException() {
        properties.setProperty(MIN, "10");
        assertThrows(AppException.class,
                () -> new IntegerTable(properties),
                "Min value must be less than or equal to max value");
    }

    @Test
    void whenMaxMoreThenMaxByteValueThenThrowException() {
        properties.setProperty(MAX, String.valueOf(Integer.MAX_VALUE + 1));
        assertThrows(AppException.class, () -> new IntegerTable(properties));
    }

    @Test
    void whenMinLessThenMinByteValueThenThrowException() {
        properties.setProperty(MIN, String.valueOf(Integer.MIN_VALUE - 1));
        assertThrows(AppException.class, () -> new IntegerTable(properties));
    }

    @Test
    void whenResultOfMultiplicationIsNotOutOfRangeThenReturnResult() {
        Table<Integer> integerTable = new IntegerTable(properties);
        int a = 1;
        int b = 2;
        assertEquals(a * b, integerTable.multiply(a, b));
    }

    @Test
    void whenResultOfMultiplicationIsOutOfRangeThenThrowException() {
        Table<Integer> byteTable = new IntegerTable(properties);
        assertThrows(AppException.class,
                () -> byteTable.multiply(Integer.MAX_VALUE, Integer.MAX_VALUE),
                "Out of range");
    }

    @Test
    void whenAddIncrementThenNumberIsIncremented() {
        Table<Integer> integerTable = new IntegerTable(properties);
        int a = 120;
        assertEquals(a + Integer.parseInt(properties.getProperty(INCREMENT)),
                integerTable.addIncrement(a, integerTable.increment));
    }

    @Test
    void whenCloneThenReturnThereAreDifferentObjectsWithTheSameValue() {
        Table<Integer> integerTable = new IntegerTable(properties);
        Integer a = 120;
        assertNotSame(a, integerTable.clone(a));
        assertEquals(a, integerTable.clone(a));
    }
}
