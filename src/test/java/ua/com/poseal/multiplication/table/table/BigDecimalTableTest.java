package ua.com.poseal.multiplication.table.table;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.com.poseal.multiplication.table.exception.AppException;

import java.math.BigDecimal;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static ua.com.poseal.multiplication.table.App.*;

class BigDecimalTableTest {
    private Properties properties;

    @BeforeEach
    void initData() {
        properties = new Properties();
        properties.setProperty(MIN, "1");
        properties.setProperty(MAX, "2");
        properties.setProperty(INCREMENT, "1");
    }

    @Test
    void whenConstructorValidThenTestPasses() {
        Table<BigDecimal> bigDecimalTable = new BigDecimalTable(properties);
        assertEquals(new BigDecimal(properties.getProperty(MIN)), bigDecimalTable.min);
        assertEquals(new BigDecimal(properties.getProperty(MAX)), bigDecimalTable.max);
        assertEquals(new BigDecimal(properties.getProperty(INCREMENT)), bigDecimalTable.increment);
    }

    @Test
    void whenConstructorNotValidThenThrowException() {
        properties.setProperty(MIN, "A");
        assertThrows(AppException.class, () -> new BigDecimalTable(properties), "Invalid properties");

        properties.setProperty(MAX, "B");
        assertThrows(AppException.class, () -> new BigDecimalTable(properties), "Invalid properties");

        properties.setProperty(INCREMENT, "C");
        assertThrows(AppException.class, () -> new BigDecimalTable(properties), "Invalid properties");
    }

    @Test
    void whenIncrementLessZeroThenThrowException() {
        properties.setProperty(INCREMENT, "-1");
        assertThrows(AppException.class,
                () -> new BigDecimalTable(properties),
                "Min value must be more than 0");
    }

    @Test
    void whenMinMoreMaxThenThrowException() {
        properties.setProperty(MIN, "10");
        assertThrows(AppException.class,
                () -> new BigDecimalTable(properties),
                "Min value must be less than or equal to max value");
    }

    @Test
    void whenResultOfMultiplicationIsNotOutOfRangeThenReturnResult() {
        Table<BigDecimal> bigDecimalTable = new BigDecimalTable(properties);
        BigDecimal a = new BigDecimal("1");
        BigDecimal b = new BigDecimal("2");
        assertEquals(a.multiply(b), bigDecimalTable.multiply(a, b));
    }

    @Test
    void whenAddIncrementThenNumberIsIncremented() {
        Table<BigDecimal> bigDecimalTable = new BigDecimalTable(properties);
        BigDecimal a = new BigDecimal("120");
        assertEquals(a.add(new BigDecimal(properties.getProperty(INCREMENT))),
                bigDecimalTable.addIncrement(a, bigDecimalTable.increment));
    }

    @Test
    void whenCloneThenReturnThereAreDifferentObjectsWithTheSameValue() {
        Table<BigDecimal> bigDecimalTable = new BigDecimalTable(properties);
        BigDecimal a = new BigDecimal("120");
        assertNotSame(a, bigDecimalTable.clone(a));
        assertEquals(a, bigDecimalTable.clone(a));
    }
}
