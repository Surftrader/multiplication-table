package ua.com.poseal.multiplication.table.table;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.com.poseal.multiplication.table.exception.AppException;

import java.math.BigInteger;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static ua.com.poseal.multiplication.table.App.*;

class BigIntegerTableTest {
    private Properties properties;

    @BeforeEach
    void initData() {
        properties = new Properties();
        properties.setProperty(MIN, "1");
        properties.setProperty(MAX, "2");
        properties.setProperty(INCREMENT, "1");
    }

    @Test
    void whenConstructorValidThenTestPasses() {
        Table<BigInteger> bigIntegerTable = new BigIntegerTable(properties);
        assertEquals(new BigInteger(properties.getProperty(MIN)), bigIntegerTable.min);
        assertEquals(new BigInteger(properties.getProperty(MAX)), bigIntegerTable.max);
        assertEquals(new BigInteger(properties.getProperty(INCREMENT)), bigIntegerTable.increment);
    }

    @Test
    void whenConstructorNotValidThenThrowException() {
        properties.setProperty(MIN, "A");
        assertThrows(AppException.class, () -> new BigIntegerTable(properties), "Invalid properties");

        properties.setProperty(MAX, "B");
        assertThrows(AppException.class, () -> new BigIntegerTable(properties), "Invalid properties");

        properties.setProperty(INCREMENT, "C");
        assertThrows(AppException.class, () -> new BigIntegerTable(properties), "Invalid properties");
    }

    @Test
    void whenIncrementLessZeroThenThrowException() {
        properties.setProperty(INCREMENT, "-1");
        assertThrows(AppException.class,
                () -> new BigIntegerTable(properties),
                "Min value must be more than 0");
    }

    @Test
    void whenMinMoreMaxThenThrowException() {
        properties.setProperty(MIN, "10");
        assertThrows(AppException.class,
                () -> new BigIntegerTable(properties),
                "Min value must be less than or equal to max value");
    }

    @Test
    void whenResultOfMultiplicationIsNotOutOfRangeThenReturnResult() {
        Table<BigInteger> bigIntegerTable = new BigIntegerTable(properties);
        BigInteger a = new BigInteger("1");
        BigInteger b = new BigInteger("2");
        assertEquals(a.multiply(b), bigIntegerTable.multiply(a, b));
    }

    @Test
    void whenAddIncrementThenNumberIsIncremented() {
        Table<BigInteger> integerTable = new BigIntegerTable(properties);
        BigInteger a = new BigInteger("120");
        assertEquals(a.add(new BigInteger(properties.getProperty(INCREMENT))),
                integerTable.addIncrement(a, integerTable.increment));
    }

    @Test
    void whenCloneThenReturnThereAreDifferentObjectsWithTheSameValue() {
        Table<BigInteger> bigIntegerTable = new BigIntegerTable(properties);
        BigInteger a = new BigInteger("120");
        assertNotSame(a, bigIntegerTable.clone(a));
        assertEquals(a, bigIntegerTable.clone(a));
    }
}
