package ua.com.poseal.multiplication.table.table;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static ua.com.poseal.multiplication.table.App.*;

class ShortTableTest {
    private Properties properties;

    @BeforeEach
    void initData() {
        properties = new Properties();
        properties.setProperty(MIN, "1");
        properties.setProperty(MAX, "2");
        properties.setProperty(INCREMENT, "1");
    }

    @Test
    void whenConstructorValidThenTestPasses() {
        Table<Short> shortTable = new ShortTable(properties);
        assertEquals(Short.parseShort(properties.getProperty(MIN)), shortTable.min);
        assertEquals(Short.parseShort(properties.getProperty(MAX)), shortTable.max);
        assertEquals(Short.parseShort(properties.getProperty(INCREMENT)), shortTable.increment);
    }

    @Test
    void whenConstructorNotValidThenThrowException() {
        properties.setProperty(MIN, "A");
        assertThrows(AppException.class, () -> new ShortTable(properties), "Invalid properties");

        properties.setProperty(MAX, "B");
        assertThrows(AppException.class, () -> new ShortTable(properties), "Invalid properties");

        properties.setProperty(INCREMENT, "C");
        assertThrows(AppException.class, () -> new ShortTable(properties), "Invalid properties");
    }

    @Test
    void whenIncrementLessZeroThenThrowException() {
        properties.setProperty(INCREMENT, "-1");
        assertThrows(AppException.class,
                () -> new ShortTable(properties),
                "Min value must be more than 0");
    }

    @Test
    void whenMinMoreMaxThenThrowException() {
        properties.setProperty(MIN, "10");
        assertThrows(AppException.class,
                () -> new ShortTable(properties),
                "Min value must be less than or equal to max value");
    }

    @Test
    void whenMaxMoreThenMaxByteValueThenThrowException() {
        properties.setProperty(MAX, String.valueOf(Short.MAX_VALUE + 1));
        assertThrows(AppException.class, () -> new ShortTable(properties));
    }

    @Test
    void whenMinLessThenMinByteValueThenThrowException() {
        properties.setProperty(MIN, String.valueOf(Short.MIN_VALUE - 1));
        assertThrows(AppException.class, () -> new ShortTable(properties));
    }

    @Test
    void whenResultOfMultiplicationIsNotOutOfRangeThenReturnResult() {
        Table<Short> shortTable = new ShortTable(properties);
        short a = 1;
        short b = 2;
        assertEquals((short) (a * b), shortTable.multiply(a, b));
    }

    @Test
    void whenResultOfMultiplicationIsOutOfRangeThenThrowException() {
        Table<Short> shortTable = new ShortTable(properties);
        assertThrows(AppException.class,
                () -> shortTable.multiply(Short.MAX_VALUE, Short.MAX_VALUE),
                "Out of range");
    }

    @Test
    void whenAddIncrementThenNumberIsIncremented() {
        Table<Short> shortTable = new ShortTable(properties);
        short a = 120;
        assertEquals((short) (a + Short.parseShort(properties.getProperty(INCREMENT))),
                shortTable.addIncrement(a, shortTable.increment));
    }

    @Test
    void whenCloneThenReturnThereAreDifferentObjectsWithTheSameValue() {
        Table<Short> shortTable = new ShortTable(properties);
        short a = 120;
        assertNotSame(a, shortTable.clone(a));
        assertEquals(a, shortTable.clone(a));
    }
}
