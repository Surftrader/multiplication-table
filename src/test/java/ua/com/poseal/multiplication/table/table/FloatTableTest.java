package ua.com.poseal.multiplication.table.table;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ua.com.poseal.multiplication.table.exception.AppException;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.*;
import static ua.com.poseal.multiplication.table.App.*;

class FloatTableTest {

    private Properties properties;

    @BeforeEach
    void initData() {
        properties = new Properties();
        properties.setProperty(MIN, "1");
        properties.setProperty(MAX, "2");
        properties.setProperty(INCREMENT, "1");
    }

    @Test
    void whenConstructorValidThenTestPasses() {
        Table<Float> floatTable = new FloatTable(properties);
        assertEquals(Float.parseFloat(properties.getProperty(MIN)), floatTable.min);
        assertEquals(Float.parseFloat(properties.getProperty(MAX)), floatTable.max);
        assertEquals(Float.parseFloat(properties.getProperty(INCREMENT)), floatTable.increment);
    }

    @Test
    void whenConstructorNotValidThenThrowException() {
        properties.setProperty(MIN, "A");
        assertThrows(AppException.class, () -> new FloatTable(properties), "Invalid properties");

        properties.setProperty(MAX, "B");
        assertThrows(AppException.class, () -> new FloatTable(properties), "Invalid properties");

        properties.setProperty(INCREMENT, "C");
        assertThrows(AppException.class, () -> new FloatTable(properties), "Invalid properties");
    }

    @Test
    void whenIncrementLessZeroThenThrowException() {
        properties.setProperty(INCREMENT, "-1");
        assertThrows(AppException.class,
                () -> new FloatTable(properties),
                "Min value must be more than 0");
    }

    @Test
    void whenMinMoreMaxThenThrowException() {
        properties.setProperty(MIN, "10");
        assertThrows(AppException.class,
                () -> new FloatTable(properties),
                "Min value must be less than or equal to max value");
    }

    @Test
    void whenMaxMoreThenMaxByteValueThenThrowException() {
        properties.setProperty(MAX, String.valueOf(Float.POSITIVE_INFINITY));
        assertThrows(AppException.class, () -> new FloatTable(properties), "Invalid properties");
    }

    @Test
    void whenMinLessThenMinByteValueThenThrowException() {
        properties.setProperty(MIN, String.valueOf(Float.NEGATIVE_INFINITY));
        assertThrows(AppException.class, () -> new FloatTable(properties), "Invalid properties");
    }

    @Test
    void whenResultOfMultiplicationIsNotOutOfRangeThenReturnResult() {
        Table<Float> floatTable = new FloatTable(properties);
        float a = 10000.0F;
        float b = 0.00001F;
        assertEquals(a * b, floatTable.multiply(a, b), 0.01);
    }

    @Test
    void whenResultOfMultiplicationIsOutOfRangeThenThrowException() {
        Table<Float> floatTable = new FloatTable(properties);
        assertThrows(AppException.class,
                () -> floatTable.multiply(Float.MAX_VALUE, Float.MAX_VALUE),
                "Out of range");
    }

    @Test
    void whenAddIncrementThenNumberIsIncremented() {
        Table<Float> floatTable = new FloatTable(properties);
        float a = 120;
        assertEquals(a + Float.parseFloat(properties.getProperty(INCREMENT)),
                floatTable.addIncrement(a, floatTable.increment));
    }

    @Test
    void whenCloneThenReturnThereAreDifferentObjectsWithTheSameValue() {
        Table<Float> floatTable = new FloatTable(properties);
        float a = 120;
        assertNotSame(a, floatTable.clone(a));
        assertEquals(a, floatTable.clone(a));
    }
}
